import { Component } from '@angular/core';
@Component({
  selector: 'loop-back',
  template:`
    <div [hidden]="false"><input #box (keyup)="0"></div>
    <div [hidden]="true"><p>{{box.value}}</p></div>
  `
})
export class LoopbackComponent { }


/*
Copyright 2016 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/